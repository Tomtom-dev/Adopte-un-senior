# :older_man: Âge d'or - FRONT :older_woman:
<img src="src/assets/logoPourAnnonce.png" alt="LogoÂged'Or" style="width:100vh;"/>

***

## Description
Le site a pour but de rassembler des associations ou des bénévoles avec des séniors pour les aider dans leurs tâches du quotidien et de favoriser la communication intergénérationnelle.

Ce projet a été généré avec Angular Cli version 12.2.2.

## Fonctionnalités


* Inscription / Connexion
* Poster une annonce, la modifier, la supprimer
* Voir les associations inscrites
* Voir toutes les annonces disponibles
* Envoyer des messages pour une annonce

***

## Demo 


![room-for-help_Screenshot1](screeCap/01.png)
![room-for-help_Screenshot2](screeCap/03.png)
![room-for-help_Screenshot3](screeCap/IllustrationGif.gif)

## Serveur de développement


Exécutez `ng serve` pour un serveur de développement. Naviguez jusqu'à `http://localhost:4200/`. L'application sera automatiquement rechargée si vous modifiez un des fichiers sources.

## Build


Lancez `ng build` pour construire le projet. Les artefacts de construction seront stockés dans le répertoire `dist/`.

## Exécution des tests unitaires


Lancer `ng test` pour exécuter les tests unitaires via [Karma](https://karma-runner.github.io)

## Technologie Utilisée


- Angular: [Angular](https://gitlab.com/Tomtom-dev/Adopte-un-senior/-/tree/develop/src/app)
- Prime-ng: [Prime-ng](https://gitlab.com/Tomtom-dev/Adopte-un-senior/-/blob/develop/src/app/caroussel/caroussel.component.ts)
- Angular-material: [Angular-material](https://gitlab.com/Tomtom-dev/Adopte-un-senior/-/blob/develop/src/app/annonce-list/annonce-filter/filter-type-service/filter-type-service.component.ts)
- Docker: [Docker](https://gitlab.com/Tomtom-dev/Adopte-un-senior/-/blob/develop/Dockerfile)

## Serveur 



liens vers le repository du serveur : https://gitlab.com/Tomtom-dev/ageOrBackEnd

## Aide supplémentaire


Pour obtenir plus d'aide sur le CLI d'Angular, utilisez `ng help` ou consultez la page [Angular CLI Overview and Command Reference](https://angular.io/cli).

***


## Auteurs


* [Heddadji Thomas](https://github.com/Tomtom-dev)
* [Dubois Guillaume](https://github.com/duboisguillaume)
* [Almoyner Héloïse](https://github.com/AlmoHelo)