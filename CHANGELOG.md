# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.2](https://gitlab.com/Tomtom-dev/Adopte-un-senior/compare/v0.0.1...v0.0.2) (2021-10-06)


### Features

* **affichage:** mise en plage de boutons pour afficher différemment la liste des annonces ([e86446a](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/e86446a5384ddf8297aea9c6b0d74e09a4a64cca))
* **affichage:** responsive tableau pour les boutons affichage ([71a1118](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/71a1118e4f28d64297bd64265991fa9713a91d07))
* **annonce:** style annonce card and responsive ([e003fb2](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/e003fb22252a701fc76763cd15397d25c8a6aa40))
* **authentification:** retrieve user data from the localStorage ([a8505c6](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/a8505c6d5b65c976e0394d9f97f93ec4dd9622b0))
* **delete:** delete file filter-option ([cb8c62c](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/cb8c62c73843ff2edf90317f4ab5186ae11a332b))
* **delete:** delete file filter-option ([2e2300a](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/2e2300a9e41384b6a7847c88a1d57f202418937d))
* **delete:** delete file filter-option ([8dffa9f](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/8dffa9f50d4d45654e1aa75867896204eb419909))
* **doc:** ajout doc sur fonction getPosts ([12b86f0](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/12b86f05a4aa982ccc999864422414be329d8122))
* **filter:** display of the applicant on the ads ([a6c743c](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/a6c743c804049cd65eee01279813ae5e1098417e))
* **filter:** filter date angular material ([74c5979](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/74c59791f890e118944f1be890d885eea840c675))
* **filter:** functional filters ([b37c677](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/b37c677f3c584f7554f74e5388f8d2469c46b571))
* **filter:** mise en place du filtre date ([6b8d9a6](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/6b8d9a678c6c02af76acf2dcd7ef212ec0f19829))
* **filter:** responsive ([1b8c90b](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/1b8c90b8ad10de84607169562c95f694abf893c5))
* **filters:** fonctionnalités pour filtrer les annonces ([d47af20](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/d47af20e19b36805a0ccd51686f9976e30b04941))
* **filters:** modif fonctionnalités filters pour gestion quand filtre non choisit ([c25a31f](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/c25a31f88588cfb181509694560f4ba2b4bd2d49))
* **filters:** modification distance filtre ([181c3ce](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/181c3cedbe864750bdd88607b0be34b278d71d76))
* **filters:** modification distance pour avoir les villes ([43d1e44](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/43d1e44cf854926023c56590a5e709cf8c25fa97))
* **filtre:** trie des villes par ordre alphabétique ([8d04a77](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/8d04a77c17200bf34f4c1780b3a7261ae8a1187b))
* **footer:** responsive ([0d8481a](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/0d8481a65a4a975dedd656d162a0488f3829d6fa))
* **inscription:** add btn tel ([941efdf](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/941efdf1d690aa5097bba8d895faacd1220dcd1b))
* **inscription:** add modification of the password buttons to hide them ([9003bee](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/9003bee727bf154785e07183414ff4c5dade5a79))
* **inscription:** all routes for inscription ([cb68580](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/cb685804881f1bd94bae8b1bf9f030e459b26965))
* **inscription:** animation check password ([6fe2b95](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/6fe2b9537207ee7024256d0661c000de59503a01))
* **inscription:** error message management for single email ([8c6efa3](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/8c6efa35d2c65065422065648330c6d079f51767))
* **menu:** setting up the menu after login ([761e8bf](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/761e8bfc07c7a6789e3390983ce4bc7a75308006))
* **navbar:** delete ondestroy in navbarcomponent ([9228712](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/9228712ec7e646fd2fd1274aa50f6c53b38d3dfe))
* **navbar:** management of the navbar when the user is connected ([261845d](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/261845ddba77daef684d57ca2fb54d95cfdf1377))
* **notannonce:** ajout d'un composant si il n'y a pas d'annonce correspondant à la recherche ([7f80d09](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/7f80d09fb4331e9e69470e21bd8ac0b2601ad311))
* **notannonce:** scss et responsive ([5a67f78](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/5a67f78a91798417e5e4b01e7b67ae5d7481f483))
* **scss:** mise en place du scss filtre annonce ([e4c7453](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/e4c74537528df2cae5336b101d3a4fbf0a443ccf))
* **signup:** route ([c99708f](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/c99708fe43c744023fe1c72f1da6e98aaa02b6a9))
* **style:** change the style of the home page ([f0d863a](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/f0d863a3f35a37350966f9b89a43bacdf663b69c))
* **style:** responsive ([1faf55a](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/1faf55a7ea0c5888c4369d73fc5450f405053e8d))
* **style:** responsive navbar connected ([d9751be](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/d9751bebbd37262e26b51b2ea450cd1654240d28))
* **style:** style version ordinateur ([ebe41b6](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/ebe41b66d9d9b3539c72b8a1513b80faf967d8a4))
* **update-annonce:** default value for the selects corresponding to the values of the announcement ([839a2fa](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/839a2fa57f9392cfaee26ac7791854f2a1e7679f))
* **update-annonce:** init form for update announcement ([8538dec](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/8538dec885cc144baffc9f1f118d5df271b51e5c))
* **update:** data recovery ([5575971](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/5575971b5b52cc2afdad0086723518c613e3699b))
* **update:** modification des filtres de la page des annonces sous forme de formulaire ([a91dfa7](https://gitlab.com/Tomtom-dev/Adopte-un-senior/commit/a91dfa7fe055a89e303520dd617f027a7de61c22))

### 0.0.1 (2021-09-10)


### Features

* **link:** ajout d'un bouton pour accéder à la page d'inscription ([bb8d087](https://github.com/Tomtom-dev/Adopte-un-senior/commit/bb8d0875eaf6ae699fee655072121b52321e4a2b))
* **onglet:** ajout d'un favicon ([ea13261](https://github.com/Tomtom-dev/Adopte-un-senior/commit/ea132618d20a7fa43b2548e9871b1190da493c90))
