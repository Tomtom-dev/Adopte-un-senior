import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CarousselComponent } from '../caroussel/caroussel.component';

import { AccueilContenuComponent } from './accueil-contenu.component';

describe('AccueilContenuComponent', () => {
  let component: AccueilContenuComponent;
  let fixture: ComponentFixture<AccueilContenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AccueilContenuComponent, CarousselComponent ],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ],
      imports : [HttpClientModule]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AccueilContenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
