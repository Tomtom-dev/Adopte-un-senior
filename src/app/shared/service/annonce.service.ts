import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Annonce } from '../models/annonce';

@Injectable({
  providedIn: 'root'
})
export class AnnonceService {



 //private options = { headers: new HttpHeaders().set('Content-Type',) };

  constructor(private http: HttpClient) { }

  /**
   * Récupère les toutes les annonces
   * @returns tableau d'annonces
   */
  public getPosts(): Observable<Annonce[]> {
    return this.http.get<Annonce[]>(`${environment.url}/annonces/`);
  }

  public addAnnonce(/*annonce: Annonce,*/ formData:FormData): Observable<Annonce> {
    console.log(formData.get('file'));
   let headers = new HttpHeaders();
  //this is the important step. You need to set content type as null
  headers.set('Content-Type', 'application/json');
 // headers.set('Accept', "multipart/form-data"); 
    /*fetch(`${environment.url}/annonces/`, {
      method: 'POST',
      body: formData
    });*/
   return this.http.post<Annonce>(`${environment.url}/annonces/`, formData, {headers});
  }
  
  public getType(): Observable<string[]> {
    return this.http.get<string[]>(`${environment.url}/annonces/types`);
  }

  public getPostByOrderDateTop5(): Observable<Annonce[]>{
    return this.http.get<Annonce[]>(`${environment.url}/annonces/top`);
  }

  public getPostById(id : number) : Observable<Annonce>{
    return this.http.get<Annonce>(`${environment.url}/annonces/${id}`)
  }

  public update(formData: FormData): Observable<Annonce>{
    let headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json')
    return this.http.put<Annonce>(`${environment.url}/annonces/`, formData, {headers});
  }

  public delete(id: Number): Observable<Annonce>{
    return this.http.delete<Annonce>(`${environment.url}/annonces/${id}`);
  }

}
