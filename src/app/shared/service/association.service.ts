import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Association } from '../models/association';

@Injectable({
  providedIn: 'root'
})
export class AssociationService {

  constructor(private http: HttpClient) { }

  public getAssociations(): Observable<Association[]> {
    return this.http.get<Association[]>(`${environment.url}/associations`);
  }

}
