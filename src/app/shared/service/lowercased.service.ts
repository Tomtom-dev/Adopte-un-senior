import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LowercasedService {

  constructor() { }

  /**
   * Mettre une string avec première lettre en majuscule et le reste en minuscule
   * @param tableau de string
   * @returns tableau de string
   */
  lowercased = (tableau:Array<any>) => {
    return tableau.map(name => name.charAt(0) + name.substring(1).toLowerCase())
  }
}
