import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Annonce } from '../models/annonce';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  public login(user: User): Observable<any> {
    return this.http.post<User>(`${environment.url}/users/login`, user);
  }
  public signupUser(user: FormData){
    let headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json')
    return this.http.post<User>(`${environment.url}/users`, user, {headers});
  }

  public getUser(id: number): Observable<User> {
    return this.http.get<User>(`${environment.url}/users/${id}`);
  }

  public getBenevoles(): Observable<User[]> {
    return this.http.get<User[]>(`${environment.url}/benevoles`);
  }

  public getBenevolesPasDansAssociation(id: number): Observable<User[]> {
    return this.http.get<User[]>(`${environment.url}/associations/${id}/benevoles`);
  }

  public addBenevole(id:number,idBenevole:number): Observable<User> {
    return this.http.post<User>(`${environment.url}/associations/${id}/benevoles/${idBenevole}`,'');
  }

  public deleteBenevole(id:number,idBenevole:number): Observable<User> {
    return this.http.delete<User>(`${environment.url}/associations/${id}/benevoles/${idBenevole}`);
  }

  public patchUser(user:FormData): Observable<User>{
    let headers = new HttpHeaders();
    headers.set('Content-Type', 'application/json')
    return this.http.patch<User>(`${environment.url}/users`, user, {headers})
  }

  public getAnnoncesById(id:number): Observable<Annonce[]>{
    return this.http.get<Annonce[]>(`${environment.url}/users/${id}/annonces`)
  }
  
  public getType(): Observable<String[]>{
    return this.http.get<String[]>(`${environment.url}/users/types`);
  }

  public getCitiesForAssociations(): Observable<String[]>{
    return this.http.get<String[]>(`${environment.url}/associations/cities`);
  }

  public getAllUsers(): Observable<User[]>{
    return this.http.get<User[]>(`${environment.url}/users/`);
  }

}
