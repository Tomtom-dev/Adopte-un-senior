import { TestBed } from '@angular/core/testing';

import { ConnectionLocalService } from './connection-local.service';

describe('ConnectionLocalService', () => {
  let service: ConnectionLocalService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConnectionLocalService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
