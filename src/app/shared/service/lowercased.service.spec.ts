import { TestBed } from '@angular/core/testing';

import { LowercasedService } from './lowercased.service';

describe('LowercasedService', () => {
  let service: LowercasedService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LowercasedService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
