import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class ConnectionLocalService {

  constructor() { }

  public isUserLoggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  connexion(isUserLoggedIn: Boolean){
    if(isUserLoggedIn){
      localStorage.setItem("Connection", "yes")
    } else {
      localStorage.removeItem("Connection")
    }
  }

  userRecovery(user: User){
    if(this.isUserLoggedIn){
      localStorage.setItem("User", JSON.stringify(user));
    } 
  }

}
