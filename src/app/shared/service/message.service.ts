import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Message } from '../models/message';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(private http: HttpClient) { }
  /**
   * Recupere les messages reçu d'un utilisateur
   */
  public getMessageUtilisateur(id: number): Observable<Message[]>{
    return this.http.get<Message[]>(`${environment.url}/users/${id}/messagesrecus`);
  }

  public delete(idMessage: Number): Observable<Message>{
    return this.http.delete<Message>(`${environment.url}/messages/${idMessage}`)
  }

  public createMessage(message: FormGroup): Observable<Message>{
    return this.http.post<Message>(`${environment.url}/messages`, message)
  }

}
