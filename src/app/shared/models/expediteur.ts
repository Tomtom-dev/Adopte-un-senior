export interface Expediteur {
    adresse:String,
    city: String,
    firstname: String,
    lastname: String,
    type: String,
    picture: String
}