export interface User{
    id: number,
    username: String,
    address: String,
    zipCode: String,
    city: String,
    createTime: Date,
    email: String,
    firstname: string,
    lastname: string,
    password: string,
    picture: string,
    type: string,
    telephone:string,
    description: string,
    membres:User[]
}