// export interface Annonce {
//     id: number,
//     timeStamp: Date,
//     type: string,
//     typeService: string,
//     date: Date,
//     title: string,
//     localisation: string,
//     description: string,
//     img: string,
//     auth_id: number
// }

import { User } from "./user";

export interface Annonce{
    id: number,
    createTime: Date,
    date: Date,
    description: string,
    location: string,
    picture: string,
    title: string,
    type: string,
    user: User
}