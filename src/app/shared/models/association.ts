export interface Association{
    id: number,
    lastname:String,
    address:String,
    zipCode:String,
    city:String,
    email:String,
    picture:String,
    telephone:String, 
    description: String
}