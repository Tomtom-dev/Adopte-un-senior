import { Expediteur } from "./expediteur";
import { User } from "./user";

export interface Message{
    id: number,
    createTime: Date,
    content:String,
    destinataire: User,
    message_precedent_id: number,
    expediteur : User,
    objet:String
}