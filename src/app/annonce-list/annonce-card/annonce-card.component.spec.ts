import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AnnonceCardComponent } from './annonce-card.component';

describe('AnnonceCardComponent', () => {
  let component: AnnonceCardComponent;
  let fixture: ComponentFixture<AnnonceCardComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnnonceCardComponent ],
      imports: [FormsModule, ReactiveFormsModule]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnonceCardComponent);
    component = fixture.componentInstance;
    component.annonce={
      "id": 1,
      "timeStamp": new Date("05/09/2021"),
      "type": "association",
      "typeService": "assistance",
      "date": new Date("2021-09-21"),
      "title": "Service civique 1 mois activités pour les séniors",
      "localisation": "Hénin-Beaumont",
      "description": "Le but est de créer des activités pour les séniors une fois par semaine pour une durée de 4h.",
      "img": "https://www.chateau-thierry.fr/sites/chateau-thierry/files/image/article/associations_0.jpg",
      "auth_id": 1
  };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
