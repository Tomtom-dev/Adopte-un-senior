import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Annonce } from 'src/app/shared/models/annonce';

@Component({
  selector: 'app-annonce-card',
  templateUrl: './annonce-card.component.html',
  styleUrls: ['./annonce-card.component.scss']
})
export class AnnonceCardComponent implements OnInit {

  @Input() annonce!: Annonce;
  @Input() affichage!: string;
  monType!: String;
  @Input() redirect!: string;
  @Input() textButton!: string;

  constructor(private route: Router) { 
  }

  ngOnInit(): void {    
    switch(this.annonce.user.type){
      case "ASSOCIATION":
        this.monType = "une association"
        break;
      case "BENEVOLE":
        this.monType = "un bénévole"
        break;
      case "SENIOR":
        this.monType = "un sénior"
        break;
      default:
        this.monType = "erreur"
    }
    
  }

  redirectToAnnonce( id: number) { 
    console.log(id);
    if(!this.redirect)
      this.route.navigate([`annonces/${id}`]);
    else
      this.route.navigate([`${this.redirect}/${id}`]);
  }
}
