import { Component, Input, OnInit } from '@angular/core';
import { Observable, observable } from 'rxjs';
import { Annonce } from '../shared/models/annonce';
import { AnnonceService } from '../shared/service/annonce.service';

@Component({
  selector: 'app-annonce-list',
  templateUrl: './annonce-list.component.html',
  styleUrls: ['./annonce-list.component.scss']
})
export class AnnonceListComponent implements OnInit {

  annonces: Annonce[] = [];
  affichage: string = "";

  constructor(private annonceService: AnnonceService) {
  }


  ngOnInit(): void {
    this.annonceService.getPosts().subscribe((annonce: Annonce[]) => {
      this.annonces = annonce.reverse();            
    })
  }

  afficher($event: string) {
    this.affichage = $event
  }


  /**
   * Fonction qui permet de renvoyer un filtre null si le filtre n'est pas utilisé par l'utilisateur
   * @param filter le filtre reçu
   * @param received le tableau de filtre
   * @returns un tableau 
   */
  testUndefined = (filter: string | undefined, received: any) => {
    if (filter === undefined || filter === "NaN-NaN-NaN") {
      received.push("")
      return received
    } else {
      return received = filter
    }
  }


  /**
   * Fonction pour rechercher les correspondances entre les filtres et les annonces
   * @param tabFilters string[] = tableau d'un des filtres choisit par l'utilisateur
   * @param value string = type de filtre
   * @param newArrayAnnonces Annonce[] = tableau vide qui recueille les annonces filtrées
   * @param tabActuel Annonce[] = tableau qui contient toutes les annonces ou les annonces filtrées par le filtre précédent
   */
  searchAnnonces = (tabFilters: string[], value: string, newArrayAnnonces: Annonce[], tabActuel: Annonce[]) => {
    tabFilters.forEach(filtre => {      
      if (Boolean(filtre) === true) {
        tabActuel.filter(ann => {
          switch (value) {
            case "type":                         
              filtre = filtre.toUpperCase();
              if (ann.user.type === filtre) {
                newArrayAnnonces.push(ann);
              }
              break;
            case "service":
              filtre = filtre.toUpperCase();
              if (ann.type === filtre) {
                newArrayAnnonces.push(ann);
              }
              break;
            case "location":           
              if (ann.location === filtre) {
                newArrayAnnonces.push(ann);
              }
              break;
            default:
              console.log("type inconnu");
          }
        })
        return newArrayAnnonces
      } else {
        newArrayAnnonces = tabActuel
        return newArrayAnnonces

      }
    })
    return newArrayAnnonces;
  }

  /**
   * Fonction pour rechercher les correspondances entre les filtres des dates et les annonces
   * @param date1 string = date de début de filtre
   * @param date2 string = date de fin de filtre
   * @param newArrayAnnonces Annonce[] = tableau vide qui recueille les annonces filtrées
   * @param tabActuel Annonce[] = tableau qui contient les annonces filtrées par le filtre précédent
   */
  searchAnnonceDate = (date1: string, date2: string, newArrayAnnonces: Annonce[], tabActuel: Annonce[]) => {
    if (date1 !== "NaN-NaN-NaN" && date2 !== "NaN-NaN-NaN") {
      tabActuel.filter(ann => {
        if (new Date(ann.date) >= new Date(date1) && new Date(ann.date) <= new Date(date2)) {
          newArrayAnnonces.push(ann);
        }
        return newArrayAnnonces
      })
      return newArrayAnnonces
    } else if (date1 !== "NaN-NaN-NaN" && date2 === "NaN-NaN-NaN") {
      tabActuel.filter(ann => {
        if (new Date(ann.date) >= new Date(date1)) {
          newArrayAnnonces.push(ann);
        }
        return newArrayAnnonces
      })
      return newArrayAnnonces
    } else if (date1 === "NaN-NaN-NaN" && date2 !== "NaN-NaN-NaN") {
      tabActuel.filter(ann => {
        if (new Date(ann.date) <= new Date(date2)) {
          newArrayAnnonces.push(ann);
        }
        return newArrayAnnonces
      })
      return newArrayAnnonces
    } else {
      newArrayAnnonces = tabActuel
      return newArrayAnnonces
    }
  }

  /**
   * Fonction qui récupère les filtres choisis pour renvoyer 
   * les annonces en conséquence
   * @param filter string qui récupère les filtres
   */
  updateAnnonce(filter: string): void {
    //remplacer par filter: string[][]
    

    this.annonceService.getPosts().subscribe((annonce: Annonce[]) => {

      let filter1 = new Array(), filter2 = new Array(), filter3 = new Array(), filter4 = new Array();
      let entites = new Array(), typesService = new Array(), cities = new Array(), dateStart = filter[3], dateEnd = filter[4];

      /**
       * Boucle 1 pour boucler sur les 4 filtres et 2e boucle pour avoir les filtres sélectionnés
       */

      // filter.forEach(element => {
      //     element.forEach(el => {
      //       console.log(el);

      //     });
      // });
      let functionEntites = this.testUndefined(filter[0], entites)
      let functionTypeService = this.testUndefined(filter[1], typesService)
      let functionCities = this.testUndefined(filter[2], cities)

      let functionFilter1 = this.searchAnnonces(functionEntites, "type", filter1, annonce)
      let functionFilter2 = this.searchAnnonces(functionTypeService, "service", filter2, functionFilter1)
      let functionFilter3 = this.searchAnnonces(functionCities, "location", filter3, functionFilter2)
      let functionFilter4 = this.searchAnnonceDate(dateStart, dateEnd, filter4, functionFilter3)

      this.annonces = functionFilter4;

    })
  }
}