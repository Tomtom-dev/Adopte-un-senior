import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Annonce } from 'src/app/shared/models/annonce';
import { AnnonceService } from 'src/app/shared/service/annonce.service';
import { UserService } from 'src/app/shared/service/user.service';

@Component({
  selector: 'app-filter-city',
  templateUrl: './filter-city.component.html',
  styleUrls: ['./filter-city.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FilterCityComponent implements OnInit {

  cities = new FormControl();
  cityList = new Array();
  @Input() type!: String;

  @Output() sendFilterCity: EventEmitter<any> = new EventEmitter();

  constructor(private serviceAnnonce: AnnonceService, private serviceUser: UserService) { }

  ngOnInit(): void {
    if(this.type === "annonce"){
      this.serviceAnnonce.getPosts().subscribe((annonces: Annonce[]) => {
        annonces.forEach(annonce => {
          if (!this.cityList.includes(annonce.location)) {
            this.cityList.push(annonce.location)
          }
        });
      })
    } else {
      this.serviceUser.getCitiesForAssociations().subscribe((cities: String[]) => {
        cities.forEach(city => {
          if (!this.cityList.includes(city)) {
            this.cityList.push(city)
          }
        });
      })
    }
  }

  filterCity(data: any) {
    this.sendFilterCity.emit(data.value)
  }
}
