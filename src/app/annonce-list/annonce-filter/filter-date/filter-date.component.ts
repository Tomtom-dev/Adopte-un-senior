import { Component, EventEmitter, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-filter-date',
  templateUrl: './filter-date.component.html',
  styleUrls: ['./filter-date.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FilterDateComponent implements OnInit {

  form: FormGroup;
  
  @Output() sendFilterDate: EventEmitter<any> = new EventEmitter();

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      start: new FormControl(""),
      end: new FormControl("")
    })
  }

  ngOnInit(): void {    
    this.sendFilterDate.emit(this.form)
  }

}
