import { Component, EventEmitter, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';
import { AnnonceService } from 'src/app/shared/service/annonce.service';
import { LowercasedService } from 'src/app/shared/service/lowercased.service';

@Component({
  selector: 'app-filter-type-service',
  templateUrl: './filter-type-service.component.html',
  styleUrls: ['./filter-type-service.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FilterTypeServiceComponent implements OnInit {

  services = new FormControl();

  serviceList!: Array<String>;

  @Output() sendFilterTypeService: EventEmitter<any> = new EventEmitter();

  constructor(private service: AnnonceService, private lower: LowercasedService) { }

  ngOnInit(): void {
    this.service.getType().subscribe((type: Array<String>) => {
      this.serviceList = this.lower.lowercased(type)    
    })
  }

  filterTypeService(data: any) {
    this.sendFilterTypeService.emit(data.value)
  }

}
