import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterTypeServiceComponent } from './filter-type-service.component';

describe('FilterTypeServiceComponent', () => {
  let component: FilterTypeServiceComponent;
  let fixture: ComponentFixture<FilterTypeServiceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilterTypeServiceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterTypeServiceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
