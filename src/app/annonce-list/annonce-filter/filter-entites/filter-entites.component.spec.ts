import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FilterEntitesComponent } from './filter-entites.component';

describe('FilterEntitesComponent', () => {
  let component: FilterEntitesComponent;
  let fixture: ComponentFixture<FilterEntitesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FilterEntitesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FilterEntitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
