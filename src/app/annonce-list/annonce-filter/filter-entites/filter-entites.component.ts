import { Component, EventEmitter, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { FormControl } from '@angular/forms';
import { LowercasedService } from 'src/app/shared/service/lowercased.service';
import { UserService } from 'src/app/shared/service/user.service';

@Component({
  selector: 'app-filter-entites',
  templateUrl: './filter-entites.component.html',
  styleUrls: ['./filter-entites.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class FilterEntitesComponent implements OnInit {

  entites = new FormControl();

  entitesList!: Array<String>;

  @Output() sendFilterEntity: EventEmitter<any> = new EventEmitter();

  constructor(private serviceUser: UserService, private lower: LowercasedService) { }

  ngOnInit(): void {
    this.serviceUser.getType().subscribe((type: Array<String>) => {
      this.entitesList = this.lower.lowercased(type);
    })
  }

  filterEntity(data: any){
    this.sendFilterEntity.emit(data.value)
  }

}
