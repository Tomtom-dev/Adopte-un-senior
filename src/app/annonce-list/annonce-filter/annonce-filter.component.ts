import { Component, EventEmitter, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Annonce } from 'src/app/shared/models/annonce';
import { AnnonceService } from 'src/app/shared/service/annonce.service';

@Component({
  selector: 'app-annonce-filter',
  templateUrl: './annonce-filter.component.html',
  styleUrls: ['./annonce-filter.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AnnonceFilterComponent implements OnInit {

  formFilter = new Array();
  localisationList = new Array();
  entities: any;
  typeService: any;
  cities: any;
  date: any;
  dateStart: any;
  dateEnd: any;
  affichage ={
    list: "list",
    block: "block"
  }
  type = "annonce";

  constructor(fb: FormBuilder, private service: AnnonceService) {  
  }


  ngOnInit(): void {
    this.service.getPosts().subscribe((annonces: Annonce[]) => {
      annonces.forEach(annonce => this.localisationList.push(annonce.location));
    })
  }

  @Output() sendFilter: EventEmitter<any> = new EventEmitter();
  @Output() sendAffichage: EventEmitter<string> = new EventEmitter();

  data!: any;

  /**
   * @param $event string qui récupère la valeur pour l'affichage
   */
  afficher($event: string){
    this.sendAffichage.emit($event)
  }


  /**
   * On récupère le tableau de filtre de entité
   * @param data Array
   */
  getFilterEntities(data: any) {
    this.entities = data
  }

  /**
   * On récupère le tableau de filtre de type de service
   * @param data Array
   */
  getFilterTypeService(data: any) {
    this.typeService = data
  }
  /**
   * On récupère le tableau de filtre de ville
   * @param data Array 
   */
  getFilterCity(data: any) {
    this.cities = data
  }
  /**
 * On récupère le tableau de filtre de date
 * @param data Array 
 */
  getFilterDate(data: any) {
    this.date = data
  }

  /**
   * Fonction qui permet d'envoyer les options choisies dans les 
   * filtres au parent
   */
  sendSearch() { 
    
    const timestampStart = Date.parse(this.date.value.start)
    const timestampEnd = Date.parse(this.date.value.end)
    const dateStart = new Date(timestampStart)
    const dateEnd = new Date(timestampEnd)
    this.dateStart = `${dateStart.getFullYear()}-${dateStart.getMonth()+1}-${dateStart.getDate()}`
    this.dateEnd = `${dateEnd.getFullYear()}-${dateEnd.getMonth()+1}-${dateEnd.getDate()}`
    
    this.data = [
      this.entities,
      this.typeService,
      this.cities,
      this.dateStart,
      this.dateEnd
    ]

    this.sendFilter.emit(this.data);
  }


}
