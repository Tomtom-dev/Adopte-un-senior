import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { AnnonceCardComponent } from './annonce-card/annonce-card.component';
import { AnnonceFilterComponent } from './annonce-filter/annonce-filter.component';

import { AnnonceListComponent } from './annonce-list.component';
import { NotAnnonceComponent } from './not-annonce/not-annonce.component';

describe('AnnonceListComponent', () => {
  let component: AnnonceListComponent;
  let fixture: ComponentFixture<AnnonceListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnnonceListComponent,AnnonceFilterComponent, AnnonceCardComponent, NotAnnonceComponent],
      imports: [ReactiveFormsModule, HttpClientModule]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnonceListComponent);
    component = fixture.componentInstance;
    component.annonces = []
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
