import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConnectionLocalService } from '../shared/service/connection-local.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  isUserLoggedIn!: boolean;

  constructor(private router:Router,  private connectionLocalService : ConnectionLocalService) { }

  ngOnInit(): void {
    this.connectionLocalService.isUserLoggedIn.subscribe( value => {
      this.isUserLoggedIn = value;
    })    
    if(localStorage.getItem("Connection") !== null && window.performance ){
      this.isUserLoggedIn = true;      
    } else {
      this.connectionLocalService.isUserLoggedIn.subscribe( value => {
        this.isUserLoggedIn = value;
      })  
    }
  }

  deconnexion(){
    this.router.navigate(['/accueil']);
    this.connectionLocalService.isUserLoggedIn.next(false);
    this.connectionLocalService.connexion(false);
    localStorage.removeItem("User");
  }
}
