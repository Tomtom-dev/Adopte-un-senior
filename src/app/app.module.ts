import { CUSTOM_ELEMENTS_SCHEMA, Injectable, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from "@angular/common/http";
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AccueilContenuComponent } from './accueil-contenu/accueil-contenu.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { ConfidentialiteComponent } from './footer/confidentialite/confidentialite.component';
import { MentionsLegalesComponent } from './footer/mentions-legales/mentions-legales.component';
import { QuiSommesNousComponent } from './footer/qui-sommes-nous/qui-sommes-nous.component';
import { AnnonceListComponent } from './annonce-list/annonce-list.component';
import { AnnonceCardComponent } from './annonce-list/annonce-card/annonce-card.component';
import { AnnonceFilterComponent } from './annonce-list/annonce-filter/annonce-filter.component';
import { CarousselComponent } from './caroussel/caroussel.component';
import { CarouselModule } from 'primeng/carousel';
import { ButtonModule } from 'primeng/button';
import { AnnonceFormComponent } from './annonce-form/annonce-form.component';
import { AuthentificationComponent } from './authentification/authentification.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import {MatButtonModule} from '@angular/material/button';
import { InscriptionComponent } from './inscription/inscription.component';
import { NotAnnonceComponent } from './annonce-list/not-annonce/not-annonce.component';

import { MatDatepickerModule } from '@angular/material/datepicker';
import { DateAdapter, MatNativeDateModule, MAT_DATE_FORMATS, NativeDateAdapter } from '@angular/material/core';
import { FilterDateComponent } from './annonce-list/annonce-filter/filter-date/filter-date.component';
import { FilterCityComponent } from './annonce-list/annonce-filter/filter-city/filter-city.component';
import { FilterTypeServiceComponent } from './annonce-list/annonce-filter/filter-type-service/filter-type-service.component';
import { FilterEntitesComponent } from './annonce-list/annonce-filter/filter-entites/filter-entites.component';
import { FileUploadComponent } from './annonce-form/file-upload/file-upload.component';
import { AnnonceDetailComponent } from './annonce-detail/annonce-detail.component';
import { AssociationListComponent } from './association-list/association-list.component';
import { AssociationCardComponent } from './association-list/association-card/association-card.component';
import { ProfilComponent } from './profil/profil.component';
import { UpdateAnnonceComponent } from './profil/update-annonce/update-annonce.component';
import { MessagerieComponent } from './messagerie/messagerie.component';
import { ContactComponent } from './annonce-detail/contact/contact.component';
import { UpdateProfilComponent } from './profil/update-profil/update-profil.component';
import { AlertDialogComponent } from './alert-dialog/alert-dialog.component';
import { MatDialogModule } from '@angular/material/dialog';
import { MessageCardComponent } from './messagerie/message-card/message-card.component';
import { MessageInfoComponent } from './messagerie/message-info/message-info.component';
import { MessageFormComponent } from './messagerie/message-form/message-form.component';


//Fonction pour modifier le format de date du DATEPICKER
const MY_DATE_FORMATS = {
  parse: {
    dateInput: { day: 'numeric', month: 'numeric', year: 'numeric' }
  },
  display: {
    dateInput: 'input',
    monthYearLabel: { year: 'numeric', month: 'short' },
    dateA11yLabel: { year: 'numeric', month: 'long', day: 'numeric' },
    monthYearA11yLabel: { year: 'numeric', month: 'long' },
  }
};
@Injectable()
export class AppDateAdapter extends NativeDateAdapter {

  /**
   * Fonction qui permet de formater la date au format FR
   * @param date Date renvoie la date au format FR
   * @param displayFormat Object récupère la date au mauvais format
   * @returns Date au bon format
   */
  format(date: Date, displayFormat: Object): string {
    if (displayFormat === 'input') {
      const day = date.getDate();
      const month = date.getMonth() + 1;
      const year = date.getFullYear();
      return `${day}/${month}/${year}`;
    } else {
      return date.toDateString();
    }
  }
}
//FIN fonction pour DATEPICKER

@NgModule({
  declarations: [
    AppComponent,
    AccueilContenuComponent,
    NavbarComponent,
    FooterComponent,
    ConfidentialiteComponent,
    MentionsLegalesComponent,
    QuiSommesNousComponent,
    AnnonceListComponent,
    AnnonceCardComponent,
    AnnonceFilterComponent,
    CarousselComponent,
    AnnonceFormComponent,
    AuthentificationComponent,
    InscriptionComponent,
    NotAnnonceComponent,
    FilterDateComponent,
    FilterCityComponent,
    FilterTypeServiceComponent,
    FilterEntitesComponent,
    FileUploadComponent,
    AnnonceDetailComponent,
    AssociationListComponent,
    AssociationCardComponent,
    ProfilComponent,
    UpdateAnnonceComponent,
    MessagerieComponent,
    ContactComponent,
    UpdateProfilComponent,
    AlertDialogComponent,
    MessageCardComponent,
    MessageInfoComponent,
    MessageFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    CarouselModule,
    ButtonModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatInputModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatIconModule,
    MatFormFieldModule,
    FormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatSelectModule,
    MatButtonModule,
    MatDialogModule
  ],
  providers: [
    { provide: DateAdapter, useClass: AppDateAdapter },
    { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS }
  ],
  bootstrap: [AppComponent],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class AppModule { }
