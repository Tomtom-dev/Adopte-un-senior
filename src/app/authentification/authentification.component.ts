import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../shared/models/user';
import { ConnectionLocalService } from '../shared/service/connection-local.service';
import { UserService } from '../shared/service/user.service';

@Component({
  selector: 'app-authentification',
  templateUrl: './authentification.component.html',
  styleUrls: ['./authentification.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AuthentificationComponent implements OnInit {

  hide = true;
  authForm: FormGroup;
  error!: String;

  constructor(private service: UserService,  private router:Router, private connectionLocalService : ConnectionLocalService) {
    this.authForm = new FormGroup({
      email: new FormControl("", [Validators.email, Validators.required]),
      password: new FormControl("", Validators.required)
    })
  }

  ngOnInit(): void {
  }

  getErrorMessage() {
    if (this.authForm.controls.email.hasError('required')) {
      return 'Veuillez entrer votre email';
    }
    return this.authForm.controls.email.hasError('email') ? 'Adresse mail invalide' : '';
  }

  login() {        
    this.service.login(this.authForm.value).subscribe((log: User) => {
      this.router.navigate(['/accueil']);
      this.connectionLocalService.isUserLoggedIn.next(true);
      this.connectionLocalService.connexion(true);
      this.connectionLocalService.userRecovery(log)
    }, (error) => {
      switch (error.status) {
        case 404:
          this.error = "Email inconnu"
          break;
        case 409:
          this.error = "Mot de passe erroné"
          break;
        default:
          this.error = "Une erreur est survenue. Veuillez réessayer"
          break;
      }
    })
  }
}
