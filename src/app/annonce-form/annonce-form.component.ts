import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Annonce } from '../shared/models/annonce';
import { User } from '../shared/models/user';
import { AnnonceService } from '../shared/service/annonce.service';
import { LowercasedService } from '../shared/service/lowercased.service';

/** @title Form field with error messages */
@Component({
  selector: 'app-annonce-form',
  templateUrl: './annonce-form.component.html',
  styleUrls: ['./annonce-form.component.scss'], encapsulation: ViewEncapsulation.None
})
export class AnnonceFormComponent implements OnInit{

  typesAnnonce!: string[];
  annonceForm: FormGroup;
  file!: File;
  user!: User;

  constructor(private annonceService: AnnonceService, private lowercase:LowercasedService, private route: Router) {
    this.annonceForm = new FormGroup({
      //email = new FormControl('', [Validators.required, Validators.email]);
      type: new FormControl('', Validators.required),
      title: new FormControl('', Validators.required),
      date: new FormControl('', Validators.required),
      location: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      picture: new FormControl(''),
      user: new FormGroup({
        id: new FormControl('')
      })
    });
  }

  ngOnInit(): void {
    this.annonceService.getType().subscribe(types => {
      this.typesAnnonce = this.lowercase.lowercased(types);
      console.log(this.typesAnnonce)
    })  

    const userJson = localStorage.getItem('User');
    this.user = userJson !== null ? JSON.parse(userJson) : {};
    this.annonceForm.get('user.id')?.setValue(this.user.id);
  }


  addAnnonce() {
    if (this.annonceForm.status === "VALID") {
      this.annonceForm.controls['type'].setValue(this.annonceForm.controls['type'].value.toUpperCase())
      const formData:FormData = new FormData();
      formData.append("img", this.file);
      formData.append("annonce", new Blob([JSON.stringify(this.annonceForm.value)], {
        type: "application/json"
    }));
      console.log(this.file)
      this.annonceService.addAnnonce(formData).subscribe((newAnnonce: Annonce) => {
        console.log(newAnnonce);
        this.route.navigate(['/annonces']);
        // this.annonceForm.reset();
      });
     /* this.annonceService.addAnnonce(this.annonceForm.value).subscribe((newAnnonce: Annonce) => {
        console.log(newAnnonce);
        this.annonceForm.reset();
      });*/
    }
    console.log(this.annonceForm);
    console.log("img formcontrol" + this.annonceForm.controls.picture.value)
  }

  getFile(file: File) {
    console.log(file)
    this.file = file;
    if (this.file) {
      console.log("titre : " + this.file.name)
    }
  }

  getErrorMessage() {
    /* if (this.email.hasError('required')) {
       return 'You must enter a value';
     }
 
     return this.email.hasError('email') ? 'Not a valid email' : '';*/
  }
}