import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AnnonceService } from '../shared/service/annonce.service';

import { AnnonceFormComponent } from './annonce-form.component';

describe('AnnonceFormComponent', () => {
  let component: AnnonceFormComponent;
  let fixture: ComponentFixture<AnnonceFormComponent>;
  let service:AnnonceService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AnnonceFormComponent],
      imports:[FormsModule, ReactiveFormsModule, MatInputModule,MatSelectModule, HttpClientModule, BrowserAnimationsModule]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AnnonceFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
     // injection du service
     service = TestBed.inject(AnnonceService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
