import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AbstractControl, FormControl, FormControlName, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-file-upload',
  templateUrl: "file-upload.component.html",
  styleUrls: ["file-upload.component.scss"]
})
export class FileUploadComponent {

  fileName = '';
  url!: any;
  alt!: String;

  @Input()
  fileControl!: FormGroup;

  @Output() sendFile: EventEmitter<any> = new EventEmitter();

  constructor(private http: HttpClient) { }

  onFileSelected(event: any) {

    if (event.target.files && event.target.files[0]) {
      let reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]);
      reader.onload = (event) => {
        if (event.target != null) {
          this.url = event.target.result;
          this.alt = event.target.result + " image sélectionnée";
        }
      }
    }

    const file: File = event.target.files[0];
    this.sendFile.emit(file);

    if (file) {
      this.fileName = file.name;
    }
    else {
      this.fileName = '';
    }

    /* if (file) {

         this.fileName = file.name;

         const formData = new FormData();

         formData.append("thumbnail", file);

         const upload$ = this.http.post("/api/thumbnail-upload", formData);

         upload$.subscribe();
     }*/
  }
}
