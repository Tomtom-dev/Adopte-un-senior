import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Message } from 'src/app/shared/models/message';
import { User } from 'src/app/shared/models/user';
import { MessageService } from 'src/app/shared/service/message.service';
import { UserService } from 'src/app/shared/service/user.service';

@Component({
  selector: 'app-message-form',
  templateUrl: './message-form.component.html',
  styleUrls: ['./message-form.component.scss']
})
export class MessageFormComponent implements OnInit {
  
  title: string = this.route.snapshot.queryParamMap.get('title') || '';
  id: string = this.route.snapshot.queryParamMap.get('destinataire') || ''
  destinataire!: User;
  type: string = this.route.snapshot.queryParamMap.get('type') || ''

  messageForm: FormGroup;
  users!: User[];
  @Input() reponse!: Message
  
  @Output() sendNewMessage: EventEmitter<any> = new EventEmitter();

  constructor(private route: ActivatedRoute, private userService: UserService, private messageService: MessageService) {
    this.messageForm = new FormGroup({
      content: new FormControl('', Validators.required),
      destinataire: new FormGroup({
        id: new FormControl('', Validators.required)
      }) ,
      expediteur: new FormControl('', Validators.required),
      objet: new FormControl('', Validators.required)
    })
  }

  ngOnInit(): void {

    this.userService.getAllUsers().subscribe((res) => {
      this.users = res;
    })

    if (this.id !== '') {
      this.userService.getUser(Number(this.id)).subscribe((res) => {
        this.destinataire = res;
      })
    }
    if(this.reponse){
      this.messageForm.controls.objet.setValue("RE : "+this.reponse.objet);
      this.messageForm.controls.destinataire.setValue({id : this.reponse.expediteur.id})
    }
    

  }

  addMessage() {
    let userJson = localStorage.getItem('User')
    let user = userJson !== null ? JSON.parse(userJson) : {};
    this.messageForm.controls.expediteur.setValue({
      id: user.id
    })
    console.log(this.messageForm);
    if(this.messageForm.status === "VALID"){
      this.messageService.createMessage(this.messageForm.value).subscribe((res) => {
        this.sendNewMessage.emit(res);
      })
    }

  }


}
