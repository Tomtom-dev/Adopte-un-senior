
import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { Message } from 'src/app/shared/models/message';

@Component({
  selector: 'app-message-info',
  templateUrl: './message-info.component.html',
  styleUrls: ['./message-info.component.scss']
})
export class MessageInfoComponent implements OnInit, OnChanges {

  @Input() message! : Message;
  @Output() reponse : EventEmitter<any> = new EventEmitter();
  
  constructor() { }

  ngOnChanges(): void {
  }

  ngOnInit(): void {
  }

  sendResponse(){
    this.reponse.emit(this.message);
  }
}
