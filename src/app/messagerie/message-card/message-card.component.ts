import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AlertDialogComponent } from 'src/app/alert-dialog/alert-dialog.component';
import { Message } from 'src/app/shared/models/message';
import { User } from 'src/app/shared/models/user';
import { MessageService } from 'src/app/shared/service/message.service';

@Component({
  selector: 'app-message-card',
  templateUrl: './message-card.component.html',
  styleUrls: ['./message-card.component.scss']
})
export class MessageCardComponent implements OnInit {


  @Input() message!: Message;
  @Input() user!: User;
  @Output() sendIdMsg: EventEmitter<any> = new EventEmitter();
  @Output() sendMsg: EventEmitter<any> = new EventEmitter();
  type: string = "message";
  MessageLu: boolean = false;
  @Input() couleur!: boolean;

  constructor(public dialog: MatDialog,
    public messageService: MessageService,
    private router: Router
  ) { }

  ngOnInit(): void {
    let userStorage = localStorage.getItem('User');
    this.user = userStorage !== null ? JSON.parse(userStorage) : {};
  }

  check: boolean = false;
  changeColor() {
    (this.check === false) ? true : false
    console.log(this.check);
  }

  messageLu() {
    this.MessageLu = true
    this.sendMsg.emit(this.message)
  }

  /*getColor(): string {
     if (this.MessageLu === true) {
      return '#9ee7dfad';
    } else {
      return '#e1e6eb';
    }
  }*/

  deleteMessage() {
    const dialogRef = this.dialog.open(AlertDialogComponent, {
      width: '300px',
      data: { type: this.type, confirmation: true }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.sendIdMsg.emit(this.message.id);
        console.log(this.message);
        
        this.messageService.delete(
          this.message.id)
          .subscribe((res: Message) => {
            this.router.navigate([`messages`])
          })
      } else {
        console.log("Annuler !!!!");
      }
    });

  }

}
