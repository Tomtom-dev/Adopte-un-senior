import { Component, OnInit } from '@angular/core';
import { Message } from '../shared/models/message';
import { User } from '../shared/models/user';
import { MessageService } from '../shared/service/message.service';

@Component({
  selector: 'app-messagerie',
  templateUrl: './messagerie.component.html',
  styleUrls: ['./messagerie.component.scss']
})
export class MessagerieComponent implements OnInit {

  user!:User;
  messages: Message[]=[];
  color="#e6ebef"
  writeMsg: Boolean = false;
  message! : Message;
  reponse! : Message;
  nbMessage!: number;
  id !:number;

  constructor(private messageService : MessageService) { }

  ngOnInit(): void {

    let userStorage = localStorage.getItem('User');
    this.user = userStorage !== null ? JSON.parse(userStorage) : {};

    this.messageService.getMessageUtilisateur(this.user.id).subscribe((message: Message[]) =>{
      this.messages= message;
      this.nbMessage = message.length;
    })
  }

  newMessageAdd(data: any){
    console.log(data);
    let userJson = localStorage.getItem('User')
    let user = userJson !== null ? JSON.parse(userJson) : {};
    if(data.destinataire.id === user.id){
      this.messages.unshift(data);
    }
    this.writeMsg = false;
    
  }

  getIdMsgForDelete(id: String){    
    this.messages.forEach(element => {
      if (element.id === Number(id)) {
        this.messages.splice(this.messages.indexOf(element),1)
      }
    })
  }

  getTargetMsg(event : Message){
    this.message = event;
    this.writeMsg = false;
    console.log(event);
  }

  getReponse(event : Message){
    this.reponse = event;
    this.writeMsg = true;
  }

  writeNewMsg(){
    this.writeMsg=true
  }

  vert(event : any, i : number){
    console.log(i);
    this.id= i;


  }

}
