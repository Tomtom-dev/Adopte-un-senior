import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CarouselModule } from 'primeng/carousel';
import { Annonce } from '../shared/models/annonce';
import { AnnonceService } from '../shared/service/annonce.service'

@Component({
  selector: 'app-caroussel',
  templateUrl: './caroussel.component.html',
  styleUrls: ['./caroussel.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CarousselComponent implements OnInit {

  annonces: Annonce[] = [];
  value!: number;

  constructor(private annonceService: AnnonceService, private route: Router) {
    if(window.innerWidth >= 1024){
      this.value = 3
    } else if(window.innerWidth >= 768 && window.innerWidth <= 1024){
      this.value = 2
    } else {
      this.value = 1
    }
  }

  ngOnInit(): void {
    this.annonceService.getPostByOrderDateTop5().subscribe((annonce: Annonce[]) => {
      this.annonces = annonce;
      console.log(annonce);
    })
  }

  redirectToAnnonce(id: String) {
    console.log(id);
    this.route.navigate([`annonces/${id}`])
  }
}
