import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AnnonceService } from '../shared/service/annonce.service';
import { CarousselComponent } from './caroussel.component';

describe('CarousselComponent', () => {
  let component: CarousselComponent;
  let fixture: ComponentFixture<CarousselComponent>;
  let service:AnnonceService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CarousselComponent],
      imports : [HttpClientModule],
      schemas: [
        CUSTOM_ELEMENTS_SCHEMA
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CarousselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
     // injection du service
     service = TestBed.inject(AnnonceService);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
