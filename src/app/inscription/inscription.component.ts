import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from '../shared/models/user';
import { UserService } from '../shared/service/user.service';
import Validation from '../validation';

interface Choice {
  value: string;
  viewValue: string;
}


@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.scss'],
  encapsulation: ViewEncapsulation.None
})


export class InscriptionComponent implements OnInit {

  hide = true;
  hide2 = true;
  file!: File;

  choices: Choice[] = [
    { value: "0", viewValue: "Une association" },
    { value: "1", viewValue: "Un bénévole" },
    { value: "2", viewValue: "Une personne agée" }
  ]
  type!: String;
  error!: String;

  authForm: FormGroup;

  constructor(private service: UserService, private router: Router) {
    this.authForm = new FormGroup({
      type: new FormControl("", Validators.required),
      lastname: new FormControl("", [Validators.minLength(3), Validators.required]),
      firstname: new FormControl(""),
      email: new FormControl("", [Validators.email, Validators.required]),
      address: new FormControl("", [Validators.minLength(3), Validators.required]),
      zipCode: new FormControl("", Validators.required),
      city: new FormControl("", Validators.required),
      telephone: new FormControl("", [
        Validators.required,
        Validators.pattern("[0]{1}[1-9]{1}[0-9]{8}")
      ]),
      password: new FormControl("", [
        Validators.required,
        Validators.pattern('((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&]).{8,30})')]),
      confMdp: new FormControl("", Validators.required),
      picture: new FormControl('')
    }, {
      validators: [Validation.match('password', 'confMdp')]
    })

  }

  choix(type: String) {
    this.type = type
  }

  ngOnInit(): void {

  }

  getErrorMessageType() {
    if (this.authForm.controls.type.value === "") {
      return "Veuillez précisez qui vous êtes"
    }
    return ""
  }

  getErrorMessageNom() {
    if (this.authForm.controls.lastname.status === "INVALID") {
      return "le nom doit avoir 3 lettres minimum"
    }
    return ""
  }

  getErrorMessagePrenom() {
    if (this.authForm.controls.firstname.status === "INVALID") {
      return "le prenom doit avoir 3 lettres minimum"
    }
    return ""
  }

  getErrorMessageEmail() {
    if (this.authForm.controls.email.status === "INVALID") {
      return "Veuillez entrer une adresse mail valide"
    }
    return ""
  }

  getErrorMessageAdresse() {
    if (this.authForm.controls.address.status === "INVALID") {
      return "Veuillez entrer une adresse"
    }
    return ""
  }

  getErrorMessageZipCode() {
    if (this.authForm.controls.zipCode.status === "INVALID") {
      return "Veuillez entrer un code postal"
    }
    return ""
  }
  getErrorMessageVille() {
    if (this.authForm.controls.city.status === "INVALID") {
      return "Veuillez entrer une ville"
    }
    return ""
  }

  getErrorMessagePhone() {
    if (this.authForm.controls.telephone.status === "INVALID") {
      return "Veuillez entrer un numéro de téléphone valide"
    }
    return ""
  }

  getErrorPassword() {
    if (this.authForm.controls.password.status === "INVALID") {
      return "Veuillez un mot de passe comportant :\n"
    }
    return ""
  }

  regMinuscule = /[a-z]/g;
  getErrorPasswordMinuscule() {
    if (this.authForm.controls.password.status === "INVALID") {
      return "\t- Une lettre minuscule minimum,\n"
    }
    return ""
  }
  regMajuscule = /[A-Z]/g;
  getErrorPasswordMajuscule() {
    if (this.authForm.controls.password.status === "INVALID") {
      return "\t- Une lettre majuscule minimum ,\n"
    }
    return ""
  }
  regNumero = /[0-9]/g;
  getErrorPasswordNumero() {
    if (this.authForm.controls.password.status === "INVALID") {
      return "\t- Un chiffre minimum ,\n"
    }
    return ''
  }
  regSpecial = /[!@#$%^&]/g
  getErrorPasswordSpeciale() {
    if (this.authForm.controls.password.status === "INVALID") {
      return "\t- Un caractère spécial minimum (!@#$%^&) ,\n"
    }
    return ""
  }
  nombre: Boolean = false;
  getErrorPasswordNombre() {
    if (this.authForm.controls.password.status === "INVALID") {
      if (this.authForm.controls.password.value.length >= 8) {
        this.nombre = true
      } else {
        this.nombre = false
      }
      return "\t- 8 caractères minimum"
    }
    return ""
  }

  subscribe() {
    if (this.type === "0") {
      this.authForm.controls['firstname'].setValidators([]);
    } else {
      this.authForm.controls['firstname'].setValidators([Validators.minLength(3), Validators.required]);
    }
    if (this.authForm.status === "VALID") {
      const formData: FormData = new FormData();
      formData.append("img", this.file);
      formData.append("user", new Blob([JSON.stringify(this.authForm.value)], {
        type: "application/json"
      }));
      this.service.signupUser(formData).subscribe((res: any) => {
        this.router.navigate(['/login']);
      }, (error) => {
        if (error.status === 409) {
          this.error = "Adresse mail est déjà prise "
        } else {
          this.error = "Un problème est survenu ! Veuillez réessayer !"
        }
      })
    }
  }
  getFile(file: File) {
    console.log(file)
    this.file = file;
    if (this.file) {
      console.log("titre : " + this.file.name)
    }
  }
}
