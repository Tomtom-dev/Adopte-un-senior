import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Association } from '../shared/models/association';
import { AssociationService } from '../shared/service/association.service';

@Component({
  selector: 'app-association-list',
  templateUrl: './association-list.component.html',
  styleUrls: ['./association-list.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AssociationListComponent implements OnInit {

  associations: Association[]=[];
  affichage: string = "";
  type = "association";
  data!: any[];
  allAssos: Association[] = [];

  constructor(private associationService: AssociationService) { }

  ngOnInit(): void {
    this.associationService.getAssociations().subscribe((association: Association[]) =>{
      this.associations = association
      this.allAssos = association
    })
  }

  affichageChange($event: string){
    this.affichage=$event;
  }

  getFilterCityInAsso(data: any[]){
    this.data = data
  }

  filtrerAssociation(){
    let newTabAssos= new Array();
    if(Boolean(this.data)){
      this.data.forEach(filter => {
        this.allAssos.forEach(asso => {
          if(asso.city === filter){
            newTabAssos.push(asso)
          }
        })
      });
      this.associations = newTabAssos;    
      return this.associations  
    }else{
      return this.associations;
    }
    
  }

}
