import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Association } from 'src/app/shared/models/association';

@Component({
  selector: 'app-association-card',
  templateUrl: './association-card.component.html',
  styleUrls: ['./association-card.component.scss']
})
export class AssociationCardComponent implements OnInit {

  @Input() association! : Association;
  @Input() affichage!: string;
  connexion: string = localStorage.getItem('Connection') || 'no';

  constructor(private route: Router) { }

  ngOnInit(): void {
    
  }

  writeMsg(){
    this.route.navigate(['/messages'], {queryParams: {destinataire: this.association.id, title: this.association.lastname, type: 'association'}})
  }

}
