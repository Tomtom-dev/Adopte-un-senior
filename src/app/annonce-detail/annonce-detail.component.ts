import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Annonce } from '../shared/models/annonce';
import { AnnonceService } from '../shared/service/annonce.service';

@Component({
  selector: 'app-annonce-detail',
  templateUrl: './annonce-detail.component.html',
  styleUrls: ['./annonce-detail.component.scss'],
  animations: [
    trigger('openClose', [
      state('true', style({ display: 'flex'})),
      state('false', style({ display: 'none'})),
      transition('* => true', animate(400)),
      transition('* => false', animate(100))
    ])
  ]
})

export class AnnonceDetailComponent implements OnInit {

  annonce!: Annonce;
  show: Boolean = false;
  showContact: String = localStorage.getItem('Connection') || 'no';

  constructor(private annonceService: AnnonceService, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    const id = this.route.snapshot.params['id'];
    this.annonceService.getPostById(id).subscribe((annonce: Annonce) => {
      this.annonce = annonce;
      console.log(this.annonce);
    })

  }

  afficherNumero() {
    this.show = true;
  }

  closeWindow(closeWindow: Boolean){
    this.show = closeWindow;
  }

}
