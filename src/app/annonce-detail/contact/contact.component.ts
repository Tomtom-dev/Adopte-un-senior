import { Route } from '@angular/compiler/src/core';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Annonce } from 'src/app/shared/models/annonce';
import { User } from 'src/app/shared/models/user';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  show: Boolean = false;

  @Input() user!: User;
  @Input() annonce!: String;
  @Output() sendCloseWindow: EventEmitter<any> = new EventEmitter();

  constructor(private route: Router) { }

  ngOnInit(): void {
    
  }

  closeWindow(){
    this.sendCloseWindow.emit(this.show);
  }

  writeMsg(){
    this.route.navigate(['/messages'])
    // this.route.navigate(['/messages'], {queryParams: {destinataire: this.user.id, title: this.annonce, type: 'annonce'}})
  }
}
