import { formatDate } from '@angular/common';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { AlertDialogComponent } from 'src/app/alert-dialog/alert-dialog.component';
import { Annonce } from 'src/app/shared/models/annonce';
import { AnnonceService } from 'src/app/shared/service/annonce.service';
import { LowercasedService } from 'src/app/shared/service/lowercased.service';

@Component({
  selector: 'app-update-annonce',
  templateUrl: './update-annonce.component.html',
  styleUrls: ['./update-annonce.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class UpdateAnnonceComponent implements OnInit {

  annonceUpdateForm: FormGroup;
  file!: File;
  serviceList!: Array<String>;
  cityList = new Array();

  type: string = "annonce";
  confirmation!: Boolean;


  constructor(private annonceService: AnnonceService, private lower: LowercasedService, private route: ActivatedRoute, private router: Router, public dialog: MatDialog) {
    this.annonceUpdateForm = new FormGroup({
      id: new FormControl(""),
      type: new FormControl('', Validators.required),
      date: new FormControl('', Validators.required),
      title: new FormControl('', Validators.required),
      location: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      picture: new FormControl(''),
      user: new FormGroup({
        id: new FormControl("")
      }),
      createTime: new FormControl('')
    });
  }

  updateAnnonce() {

    const id = this.route.snapshot.params['id']

    this.annonceUpdateForm.patchValue({
      id: id,
      type: this.annonceUpdateForm.value.type.toUpperCase(),
    })

    if (this.annonceUpdateForm.status === "VALID") {
      const formData: FormData = new FormData();
      formData.append("img", this.file);
      formData.append("annonce", new Blob([JSON.stringify(this.annonceUpdateForm.value)], {
        type: "application/json"
      }))
      this.annonceService.update(formData).subscribe((res: any) => {
        console.log(res);
        this.router.navigate(['profil'])
      })
    }
  }

  updateFile(file: File) {
    this.file = file;
  }

  deleteAnnonce() {
    const dialogRef = this.dialog.open(AlertDialogComponent, {
      width: '300px',
      data: { type: this.type, confirmation: true }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.annonceService.delete(this.route.snapshot.params['id']).subscribe((res: Annonce) => {
          this.router.navigate([`profil`])
        })
      } else {
        console.log("Annuler !!!!");
      }
    });
  }

  ngOnInit(): void {
    this.annonceService.getType().subscribe((type: Array<String>) => {
      this.serviceList = this.lower.lowercased(type)
    })
    this.annonceService.getPosts().subscribe((annonces: Annonce[]) => {
      annonces.forEach(annonce => {
        if (!this.cityList.includes(annonce.location)) {
          this.cityList.push(annonce.location)
        }
      });
    })

    const id = this.route.snapshot.params['id']

    this.annonceService.getPostById(id).subscribe((annonce: Annonce) => {
      this.annonceUpdateForm.controls['id'].setValue(annonce.id);
      this.annonceUpdateForm.controls['title'].setValue(annonce.title);
      this.annonceUpdateForm.controls['location'].setValue(annonce.location);
      this.annonceUpdateForm.controls['description'].setValue(annonce.description);
      this.annonceUpdateForm.controls['createTime'].setValue(annonce.createTime);
      this.annonceUpdateForm.controls['date'].setValue(formatDate(annonce.date,'yyyy-MM-dd','en'));
      this.annonceUpdateForm.controls['type'].setValue(annonce.type.charAt(0) + annonce.type.substring(1).toLowerCase());
      this.annonceUpdateForm.controls.user.setValue({
        id: annonce.user.id
      })
      
      this.annonceUpdateForm.controls['picture'].setValue(annonce.picture);
    })

  }

}
