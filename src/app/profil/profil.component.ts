import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { AlertDialogComponent } from '../alert-dialog/alert-dialog.component';
import { Annonce } from '../shared/models/annonce';
import { User } from '../shared/models/user';
import { UserService } from '../shared/service/user.service';
export interface DialogData {
  animal: string;
  name: string;
}

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ProfilComponent implements OnInit {

  user!:User;
  annonces: Annonce[] = [];
  type: string = "profil";
  confirmation!: Boolean;

  constructor(private service: UserService, private route: Router,public dialog: MatDialog) {

  }
  ngOnInit(): void {
    const userJson = localStorage.getItem('User');
    this.user = userJson !== null ? JSON.parse(userJson) : {};
    this.service.getUser(this.user.id).subscribe(result => {
      this.user = result;
    })
    console.log(this.user);
    
    this.service.getAnnoncesById(this.user.id).subscribe(result =>{
      this.annonces = result;
    })
  }


}
