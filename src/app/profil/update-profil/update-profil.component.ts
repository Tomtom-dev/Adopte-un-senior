import { Route } from '@angular/compiler/src/core';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/shared/models/user';
import { UserService } from 'src/app/shared/service/user.service';

@Component({
  selector: 'app-update-profil',
  templateUrl: './update-profil.component.html',
  styleUrls: ['./update-profil.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class UpdateProfilComponent implements OnInit {

  userForm: FormGroup;
  user!: User;
  error!: String;
  membres!: User[];
  benevoles: User[] = [];
  benevoleForm!: FormGroup;
  file!: File;

  constructor(private service: UserService, private route: Router) {
    this.userForm = new FormGroup({
      id: new FormControl(""),
      lastname: new FormControl("", [Validators.minLength(3), Validators.required]),
      firstname: new FormControl(""),
      email: new FormControl("", [Validators.email, Validators.required]),
      address: new FormControl("", [Validators.minLength(3), Validators.required]),
      zipCode: new FormControl("", Validators.required),
      city: new FormControl("", Validators.required),
      telephone: new FormControl("", [
        Validators.required,
        Validators.pattern("[0]{1}[1-9]{1}[0-9]{8}")
      ]),
      picture: new FormControl('')
    })
  }

  ngOnInit(): void {
    const userJson = localStorage.getItem('User');
    this.user = userJson !== null ? JSON.parse(userJson) : {};
    this.service.getUser(this.user.id).subscribe(result => {
      this.user = result;
      this.membres = result.membres;
    })
    this.userForm.controls['id'].setValue(this.user.id);
    this.userForm.controls['lastname'].setValue(this.user.lastname);
    this.userForm.controls['firstname'].setValue(this.user.firstname);
    this.userForm.controls['email'].setValue(this.user.email);
    this.userForm.controls['address'].setValue(this.user.address);
    this.userForm.controls['zipCode'].setValue(this.user.zipCode);
    this.userForm.controls['city'].setValue(this.user.city);
    this.userForm.controls['telephone'].setValue(this.user.telephone);

    if (this.user.type === 'ASSOCIATION') {
      this.service.getBenevolesPasDansAssociation(this.user.id).subscribe(result => {
        this.benevoles = result;
      })
      this.benevoleForm = new FormGroup({
        id: new FormControl('')
      });
    }
  }


  getErrorMessageNom() {
    if (this.userForm.controls.lastname.status === "INVALID") {
      return "le nom doit avoir 3 lettres minimum"
    }
    return ""
  }

  getErrorMessagePrenom() {
    if (this.userForm.controls.firstname.status === "INVALID") {
      return "le prenom doit avoir 3 lettres minimum"
    }
    return ""
  }

  getErrorMessageEmail() {
    if (this.userForm.controls.email.status === "INVALID") {
      return "Veuillez entrer une adresse mail valide"
    }
    return ""
  }

  getErrorMessageAdresse() {
    if (this.userForm.controls.address.status === "INVALID") {
      return "Veuillez entrer une adresse valide"
    }
    return ""
  }

  getErrorMessageVille() {
    if (this.userForm.controls.city.status === "INVALID") {
      return "Veuillez entrer une ville valide"
    }
    return ""
  }

  getErrorMessageZipCode() {
    if (this.userForm.controls.zipCode.status === "INVALID") {
      return "Veuillez entrer un code postal valide"
    }
    return ""
  }


  getErrorMessagePhone() {
    if (this.userForm.controls.telephone.status === "INVALID") {
      return "Veuillez entrer un numéro de téléphone valide"
    }
    return ""
  }

  subscribe() {
    if (this.user.type === "ASSOCIATION") {
      this.userForm.controls['firstname'].setValidators([]);
    } else {
      this.userForm.controls['firstname'].setValidators([Validators.minLength(3), Validators.required]);
    }
    if (this.userForm.status === "VALID") {
      const formData: FormData = new FormData();
      formData.append("img", this.file);
      formData.append("user", new Blob([JSON.stringify(this.userForm.value)], {
        type: "application/json"
      }));
      this.service.patchUser(formData).subscribe((res: any) => {
        this.error = "";
        this.route.navigate(['/profil']);
      }, (error) => {
        if (error.status === 409) {
          this.error = "Adresse mail est déjà prise "
        } else {
          this.error = "Un problème est survenu ! Veuillez réessayer !"
        }
      })
    }
  }

  getFile(file: File) {
    console.log(file)
    this.file = file;
    if (this.file) {
      console.log("titre : " + this.file.name)
    }
  }

  addBenevole() {
    const id = this.benevoleForm.controls.id.value;

    if (id) {
      this.benevoles.forEach(element => {
        if (element.id === id) {
          this.benevoles.splice(this.benevoles.indexOf(element), 1)
          this.membres.push(element)
        }
      })
      this.service.addBenevole(this.user.id, id).subscribe(result => {
        console.log(result);
      })
    }
  }
  deleteBenevoleInAssos(id: number) {
    this.membres.forEach(element => {
      if (element.id === id) {
        this.membres.splice(this.membres.indexOf(element), 1)
        this.benevoles.push(element)
      }
    })
    this.service.deleteBenevole(this.user.id, id).subscribe(result => {
      console.log(result);
    })
  }
}
